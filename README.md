Ce site est la vitrine temporaire de l'association **Grimpe13 / Glace13**. Le résultat est visible sur **http://glace13.gitlab.io/glace13-web/**.

Il utilise le générateur de site statique [Hugo] et est hébergé sur les [GitLab Pages](https://pages.gitlab.io). Grâce à [l'intégration continue](https://about.gitlab.com/gitlab-ci/), il est reconstruit à chaque *commit*. 

Le thème employé est [Dimension](https://github.com/your-identity/hugo-theme-dimension), produit par [Davide Asnaghi](https://asnaghi.me/) pour [html5up](), l'image de fond a été produite par [Kurt Cotoaga](https://unsplash.com/@kydroon). 

Le *contenu* de ce site est diffusé sous licence CC-BY-NC.

## Déploiement local

Pour déployer ce site localement :

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server` and visit `localhost:1313/hugo/`
1. Add content
1. Generate the website: `hugo` (optional)

Un peu plus d'info sur la [documentation][].

## Contenu 

le contenu du site est situé dans : 

- le répertoire [content](content/) pour tout le texte : un fichier markdown par item.
- le répertoire [statis](static/) pour les images

Le thème `Dimension` a également été légèrement modifié ([pied de page](https://gitlab.com/glace13/glace13-web/-/commit/808eda84125c7c28be15ea1e2695fddc921894a4) et logo).

## Group Pages

Ce projet appartient au groupe Glace13, et il est donc hébergé sur `http://glace13.gitlab.io/glace13-web/`


[hugo]: https://gohugo.io
[html5up]: https://html5up.net
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
