---
title: Orga
---

## Pour qui ?

Pour tous les membres de l'association et [les voisins](https://www.fsgt.org/activites/escal_mont/idfgrimper-actu/grimper-chez-les-voisins). 

## Quand ?

chaque jour de 18h30 à 22h et le weekend de 10h à 20h.

## Comment s'inscrire ?

Page à venir.