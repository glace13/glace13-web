---
title: Le mur
---

## Au coeur du quartier Glacière

Situé au 121 rue de la Glacière et faisant partie du gymnase Glacière, le mur a été livré en 2021 à la Mairie.

## Publics

Le mur est fréquenté par des scolaires durant la journée, par les membres de l'association sur les autres créneaux.

![une photo](mur-total.jpg)