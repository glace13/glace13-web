---
title: Crédits
---

Ce site est produit avec [Hugo](https://gohugo.io/), il est hébergé par [GitLab](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/). 

Le thème employé est [Dimension](https://github.com/your-identity/hugo-theme-dimension), produit par [Davide Asnaghi](https://asnaghi.me/), l'image de fond a été produite par [Kurt Cotoaga](https://unsplash.com/@kydroon). 

Le contenu de ce site est diffusé sous licence CC-BY-NC.