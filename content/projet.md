---
title: Le projet
---

## Un projet associatif et sportif

Grimpe13 / Glace13 est une association affiliée à la [FSGT](https://www.fsgt.org/federal/Presentation) qui a pour objectif de promouvoir la pratique de l'escalade dans le quartier de Glacière à Paris 13.
